﻿using UnityEngine;
using System.Collections;

public class pieceInteract : MonoBehaviour {

	public pieceControl control;
	public bool destroyer = true;
	public int value = 0;
	public int multiply = 0;
	public bool chain = false;

	public bool explodeLeft = false;
	public bool explodeRight = false;
	public bool explodeUp = false;
	public bool explodeDown = false;
	public bool killed=false;

	public GameObject explosion;

	public GameObject line;
	
	public bool used = false;

	public GameObject scorePiece;
	
	public bool valuePiece;
	public bool voidPiece;

	void Awake() {
		//get the control scripts
		control = GameObject.Find("pieceControl").GetComponent<pieceControl>();
		if(!valuePiece){
			control.pieceCount-=1;
		} else {
			control.valuePieceCount+=1;
		}
	}


	void OnMouseOver(){
		if(control!=null && control.swiping && control.canSwipe2){
		//if a tile has already been tapped, check to see if this one is next to it [simple distance]
		//if(control.count>0 && control.count<control.maxSwipe){
			if(control.lastTile!=null){
				if(Vector3.Distance(this.transform.position,control.lastTile.transform.position)<1.0f){
					if(!used){
						if(control.count<control.maxSwipe){
							control.lastTile=this.gameObject;
							control.tiles.Add(this.gameObject);
							if(multiply>0){
								print("adding");

								if (control.multiplyB == 0) {
									control.multiplyB = multiply;
								} else {

									if (multiply > 0)
										control.multiplyB *= multiply;
								}

							}
							showValues();

							if(!chain){
								control.count+=1;
							}

							used=true;
							//GetComponent<Renderer>().material.color = Color.black;
							GameObject clone;
							clone = Instantiate(line, transform.position, transform.rotation) as GameObject;
							clone.transform.parent = GameObject.Find("tracer").transform;

							GameObject.Find("bling").GetComponent<AudioSource>().Play();
						}
					} else {
						// remove piece from swipe list
						if (control.tiles.Count>1 && this.gameObject == control.tiles[control.tiles.Count-2]) {
							// piece was swiped before - go back to that one
							GameObject removedTile = control.tiles[control.tiles.Count-1];
							pieceInteract removedTileScript = removedTile.GetComponent<pieceInteract>();

							control.tiles.RemoveAt(control.tiles.Count-1);
							control.lastTile=control.tiles[control.tiles.Count-1];
							
							if(removedTileScript.multiply>0){
								print("remove multiplier");

								if (control.multiplyB == removedTileScript.multiply) {
									control.multiplyB = 0;
								} else {
									control.multiplyB /= removedTileScript.multiply;
								}
							}
							if (removedTileScript.value > 0) {
								removeValueLabel();
							}

							if(!removedTileScript.chain){
								control.count-=1;
							}

							removedTileScript.used=false;
							
							GameObject tracer = GameObject.Find("tracer");
							Transform lastChild = tracer.transform.GetChild(tracer.transform.childCount-1);
							Destroy(lastChild.gameObject);
						}
					}
				}
			}
			else{
				if(!used){
					control.lastTile=this.gameObject;
					control.tiles.Add(this.gameObject);
					if(multiply>0){
						print("adding");

						if (control.multiplyB == 0) {
							control.multiplyB = multiply;
						} else {
							if (multiply > 0)
								control.multiplyB *= multiply;
						}
					}

					//showing the messages

					if(control.count>1){
						GameObject.Find("sum").GetComponent<TextMesh>().text+=" + ";
					}
					showValues();
					
					used=true;
					//GetComponent<Renderer>().material.color = Color.black;
					GameObject clone;
					clone = Instantiate(line, transform.position, transform.rotation) as GameObject;
					clone.transform.parent = GameObject.Find("tracer").transform;
					
					GameObject.Find("bling").GetComponent<AudioSource>().Play();
				}
			}
		}
	}

	//creating the number bar at the top of the screen
	public void showValues(){

		if(value>0){
			if(control.multiplyB>0){
				GameObject.Find("sum").GetComponent<TextMesh>().text+=" " + value*control.multiplyB;
			}
			if(control.multiplyB==0){
				GameObject.Find("sum").GetComponent<TextMesh>().text+=" " + value;
			}
		}

		GameObject clone;
		//clone = Instantiate(scorePiece, transform.position, transform.rotation) as GameObject;
	}

	private void removeValueLabel() {
		TextMesh textMesh = GameObject.Find("sum").GetComponent<TextMesh>();
		int lastIndex = textMesh.text.LastIndexOf(" ");
		if (lastIndex == -1) {
			textMesh.text = "";
		} else {
			textMesh.text = textMesh.text.Substring(0, lastIndex);
		}
	}

	
	public void SetVoid() {
		/*
		destroyer = false;
		value = 0;
		multiply = 0;
		chain = false;
		explodeLeft = false;
		explodeDown = false;
		explodeRight = false;
		explodeUp = false;
		killed = false;
		used = false;
		valuePiece = false;
		voidPiece = true;

		GetComponent<Collider>().enabled = true;
		*/
		
		levelMaker maker = GameObject.Find("levelMaker").GetComponent<levelMaker>();
		GameObject clone = Instantiate(maker.pieces[2], transform.position, transform.rotation) as GameObject;
		clone.name = "tile" + clone.transform.position.x + "_" + clone.transform.position.y;
		Destroy(this.gameObject);
	}

	public void kill2(){
		GetComponent<Renderer>().material.color = Color.white;
		if(!killed){
			killed=true;
			if(explodeDown){
				string name1 = "tile" + (transform.position.x) + "_" + (transform.position.y-0.85f);
				if(GameObject.Find(name1)!=null && GameObject.Find(name1).GetComponent<pieceInteract>().killed==false){
					GameObject.Find(name1).GetComponent<pieceInteract>().kill2();
					GameObject.Find("explosion").GetComponent<AudioSource>().Play();
				}
			}
			if(explodeUp){
				string name2 = "tile" + (transform.position.x) + "_" + (transform.position.y+0.85f);
				if(GameObject.Find(name2)!=null && GameObject.Find(name2).GetComponent<pieceInteract>().killed==false){
					GameObject.Find(name2).GetComponent<pieceInteract>().kill2();
					GameObject.Find("explosion").GetComponent<AudioSource>().Play();
				}
			}
			if(explodeRight){
				string name3 = "tile" + (transform.position.x+0.85f) + "_" + (transform.position.y);
				if(GameObject.Find(name3)!=null && GameObject.Find(name3).GetComponent<pieceInteract>().killed==false){
					GameObject.Find(name3).GetComponent<pieceInteract>().kill2();
					GameObject.Find("explosion").GetComponent<AudioSource>().Play();
				}
			}
			if(explodeLeft){
				string name4 = "tile" + (transform.position.x-0.85f) + "_" + (transform.position.y);
				if(GameObject.Find(name4)!=null && GameObject.Find(name4).GetComponent<pieceInteract>().killed==false){
					GameObject.Find(name4).GetComponent<pieceInteract>().kill2();
					GameObject.Find("explosion").GetComponent<AudioSource>().Play();
				}
			}

			if (valuePiece) {
				control.valuePieceDestroyed+=1;
			}
			if(destroyer){
				control.pieceDestroyed+=1;

				//make an explosion
				GameObject clone;
				clone = Instantiate(explosion, transform.position, transform.rotation) as GameObject;
				Camera.main.GetComponent<cameraShake>().Shake(0.15f,0.01f);
				GetComponent<Renderer>().enabled=false;
			}
			SetVoid();
		}
	}

	public void kill(){

		if(!destroyer){
			//make an explosion
			GameObject clone;
			clone = Instantiate(explosion, transform.position, transform.rotation) as GameObject;
			Camera.main.GetComponent<cameraShake>().Shake(0.15f,0.01f);
		}

		GetComponent<Renderer>().material.color = Color.white;
		if(!killed){

			killed=true;
			//doing the bomb functions
			if(explodeDown){
				string name1 = "tile" + (transform.position.x) + "_" + (transform.position.y-0.85f);
				if(GameObject.Find(name1)!=null && GameObject.Find(name1).GetComponent<pieceInteract>().killed==false){
					GameObject.Find(name1).GetComponent<pieceInteract>().kill2();
					Camera.main.GetComponent<cameraShake>().Shake(0.25f,0.01f);
					GameObject.Find("explosion").GetComponent<AudioSource>().Play();
				}
			}
			if(explodeUp){
				string name2 = "tile" + (transform.position.x) + "_" + (transform.position.y+0.85f);
				if(GameObject.Find(name2)!=null){
					if(GameObject.Find(name2).GetComponent<pieceInteract>().killed==false){
						GameObject.Find(name2).GetComponent<pieceInteract>().kill2();
						Camera.main.GetComponent<cameraShake>().Shake(0.25f,0.01f);
						GameObject.Find("explosion").GetComponent<AudioSource>().Play();
					}
				}
			}
			if(explodeRight){
				string name3 = "tile" + (transform.position.x+0.85f) + "_" + (transform.position.y);
				if(GameObject.Find(name3)!=null && GameObject.Find(name3).GetComponent<pieceInteract>().killed==false){
					GameObject.Find(name3).GetComponent<pieceInteract>().kill2();
					Camera.main.GetComponent<cameraShake>().Shake(0.25f,0.01f);
					GameObject.Find("explosion").GetComponent<AudioSource>().Play();
				}
			}
			if(explodeLeft){
				string name4 = "tile" + (transform.position.x-0.85f) + "_" + (transform.position.y);
				if(GameObject.Find(name4)!=null && GameObject.Find(name4).GetComponent<pieceInteract>().killed==false){
					GameObject.Find(name4).GetComponent<pieceInteract>().kill2();
					Camera.main.GetComponent<cameraShake>().Shake(0.25f,0.01f);
					GameObject.Find("explosion").GetComponent<AudioSource>().Play();
				}
			}

			if (control.multiply == 0) {
				control.multiply = multiply;
			} else {
				if (multiply > 0)
					control.multiply *= multiply;
			}

			if(value>0){
				if(control.multiply>0){
					//control.score+=value*control.multiply;
					control.added+=value*control.multiply;
				}
				else{
					//control.score+=value;
					control.added+=value;
				}
			}

			if (valuePiece) {
				control.valuePieceDestroyed+=1;
			}
			if(destroyer){
				control.pieceDestroyed+=1;

				//make an explosion
				GameObject clone;
				clone = Instantiate(explosion, transform.position, transform.rotation) as GameObject;
				Camera.main.GetComponent<cameraShake>().Shake(0.15f,0.01f);
				//Destroy(gameObject);
				GetComponent<Renderer>().enabled=false;
			}

			SetVoid();
		}
	}
}