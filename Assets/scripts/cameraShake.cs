using UnityEngine;
using System.Collections;
public class cameraShake : MonoBehaviour
{
   private Vector3 originPosition;
   private Quaternion originRotation;
   public float shake_decay;
   public float shake_intensity;
 
   void Update (){
      if (shake_intensity > 0){
         transform.localPosition = originPosition + Random.insideUnitSphere * shake_intensity;
         shake_intensity -= shake_decay;
      }
   }
 
   public void Shake(float shake_intensity2, float shake_decay2){
	  shake_intensity = shake_intensity2;
	  shake_decay = shake_decay2;
      originPosition = Vector3.zero;
   }
}