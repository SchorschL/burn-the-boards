﻿using UnityEngine;
using System.Collections;

public class testingShortcuts : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnMouseDown() {
        pieceControl control = GameObject.Find("pieceControl").GetComponent<pieceControl>();
        if (this.gameObject.name == "1star-shortcut") {
            Debug.Log("take 1 star shortcut");
            control.score = 0;
        }

        if (this.gameObject.name == "2star-shortcut") {
            Debug.Log("take 2 star shortcut");
            control.score = control.silver;
        }

        if (this.gameObject.name == "3star-shortcut") {
            Debug.Log("take 3 star shortcut");
            control.score = control.gold;
        }
		control.noAnalytics = true;
		control.valuePieceDestroyed = control.valuePieceCount;
		// control.complete = true;
    }
}
