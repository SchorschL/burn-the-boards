﻿using UnityEngine;
using System.Collections;

public class button : MonoBehaviour {

	public string message;
	public GameObject home;
	public string instruction;
	public TextMesh floop;

	void Start(){
		StartCoroutine(floop.GetComponent<wrapText>().loadLine(instruction));
	}

	void OnMouseDown(){
		home.SendMessage(message);
	}
}