﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class levelMaker : MonoBehaviour {

	public Vector2 size = new Vector2(9,9);
	public GameObject[] pieces;
	public int level = 1;
	public int zone  = 1;
	
	public string specificLevel = "";

	public int rotations = 0;

	public Vector3 hitboxSize = new Vector3(0.8f,0.8f,0.2f);

	private int[,] levelMatrix;

	private global control;


	private Dictionary<int, int> rotationTable = new Dictionary<int, int>()
	{
	    {16, 19}, // down to left
	    {19, 26}, // left to up
	    {26, 25}, // up to right
	    {25, 16}, // right to down
	    {17, 28}, // downleft to upleft
	    {28, 29}, // upleft to upright
	    {29, 18}, // upright to downright
	    {18, 17}, // downright to downleft
	    {27, 20}, // updown to leftright
	    {20, 27}, // leftright to updown
	    {21, 22}, // nodown to noleft
	    {22, 24}, // noleft to noup
	    {24, 23}, // noup to noright
	    {23, 21}  // noright to nodown
	};

	private Dictionary<int, int> boardsPerBatch = new Dictionary<int, int>()
	{
	    {1, 10}, 
	    {2, 10}, 
	    {3, 10}, 
	    {4, 10}, 
	    {5, 10}, 
	    {6, 10}, 
	    {7, 10}, 
	    {8, 10}, 
	    {9, 10}, 
	    {10, 10}, 
	    {11, 10}, 
	    {12, 10}, 
	    {13, 10}, 
	    {14, 10}
	};

	
	// Use this for initialization
	void Start () {
	
		//get control to know which pack to load a level from
		control = GameObject.Find("Global").GetComponent<global>();

		//TEMP MENU - load the level from the control
		zone = control.rank;
		// zone = 8;
		//level = 10;
		//level = control.level;

		//choose a random level from the zone pack
		rotations = 0;
		level = Random.Range(1,boardsPerBatch[zone]+1);
		List<string> playedBoards = control.PlayedBoardsInBatch(zone);
		Debug.Log("played boards: " + playedBoards.Count);
		if (playedBoards.Count < boardsPerBatch[zone]) {
			// pick random level that was not already played
			do {
	        	level = Random.Range(1,boardsPerBatch[zone]+1);
	        } while (control.playedBoards.Contains(zone+"_"+level+"r"+rotations));
		} else {
			// check if endgame level selection
			bool endgame = false;
			if (control.rank >= control.rankUp.Length+1) {
				endgame = true;
				playedBoards = control.playedBoards;
			}
			// pick rotated level
			int i = 0;
			do {
				if (endgame) {
					zone = Random.Range(9,15);
	        	}
	        	level = Random.Range(1,boardsPerBatch[zone]+1);
	        	rotations = Random.Range(1,3);
	        	i++;
	        } while (control.playedBoards.Contains(zone+"_"+level+"r"+rotations) && i<100);
	        if (i==100) {
	        	// pick any level of any rotation (this shouldn't even happen)
	        	level = Random.Range(1,boardsPerBatch[zone]+1);
	        	rotations = Random.Range(1,3);
	        }
		}

		//load the tutorial level for this pack if required
		if(control.tutorial){
			control.tutorial=false;
			level = 1;
		}

        Debug.Log("play " + zone+"_"+level+"r"+rotations);
		control.playedBoards.Add(zone+"_"+level+"r"+rotations);

		// analytics
		GA.API.Design.NewEvent("Work:" + zone + "_" + level + ":Start", Time.time);

		//load the text file for the correct level
		string loader = zone + "_" + level;

		print(loader);


		TextAsset levelText = (TextAsset)Resources.Load(loader, typeof(TextAsset));

		string[] splitter = levelText.text.Split(',');
		
		levelMatrix = new int[(int)size.x, (int)size.y];
		// transfer level from text file to a 2d array
		for (int x=0; x<size.x; x++) {
			for (int y=0; y<size.y; y++) {
				levelMatrix[x,y] = int.Parse(splitter[y+(x*9)]);
			}
		}
	
		for (int i=0; i<rotations; i++) {
			int[,] rotatedMatrix = RotateMatrix(levelMatrix, (int)size.x);
			levelMatrix = rotatedMatrix;
		}
		
		GameObject.Find("pieceControl").GetComponent<pieceControl>().gold = int.Parse(splitter[splitter.Length-1]);
		GameObject.Find("pieceControl").GetComponent<pieceControl>().silver = int.Parse(splitter[splitter.Length-2]);

		int count = 0;

		for(int x = 0; x < size.x; x++)
		{
			for(int y = 0; y < size.y; y++)
			{
				//convert a letter to a number (just do this manually) - copy and past for full coe set
				/*
				if(int.Parse(splitter[(x*8)+y]=="A"){
					clone = Instantiate(pieces[int.Parse(splitter[(x*8)+y])], new Vector3(y,x,0), transform.rotation) as GameObject;
					clone.name = "tile" + x + "_" + y;
					count+=1;
				}*/


				//add a piece
				GameObject clone;
				if(levelMatrix[x,y]>0){
					clone = Instantiate(pieces[levelMatrix[x,y]], new Vector3((y*0.85f),(9-x)*0.85f,0), transform.rotation) as GameObject;
					clone.name = "tile" + clone.transform.position.x + "_" + clone.transform.position.y;

					// if screw, disable collider
					if (levelMatrix[x,y]==7) {
						clone.GetComponent<Collider>().enabled = false;
					}

					// make hitboxes smaller
					Vector3 colliderSize = hitboxSize;
					clone.GetComponent<BoxCollider>().size = colliderSize;

					count+=1;
					foreach (Transform child in clone.transform) {
						if (child.name == "up") {
							Vector3 pos = new Vector3(child.transform.position.x, child.transform.position.y, -1);
							child.transform.position = pos;
						}
						if (child.name == "left") {
							Vector3 pos = new Vector3(child.transform.position.x, child.transform.position.y, -1);
							child.transform.position = pos;
						}
						if (child.name == "right") {
							Vector3 pos = new Vector3(child.transform.position.x, child.transform.position.y, -1);
							child.transform.position = pos;
						}
						if (child.name == "down") {
							Vector3 pos = new Vector3(child.transform.position.x, child.transform.position.y, -1);
							child.transform.position = pos;
						}
					}
							
				}

				//adding an empty slot thing
				if(levelMatrix[x,y]==0){
					clone = Instantiate(pieces[2], new Vector3((y*0.85f),(9-x)*0.85f,0), transform.rotation) as GameObject;
					clone.name = "tile" + clone.transform.position.x + "_" + clone.transform.position.y;
					count+=1;

					// make hitboxes smaller
					Vector3 colliderSize = hitboxSize;
					clone.GetComponent<BoxCollider>().size = colliderSize;
				}

			}
		}

		GameObject.Find("pieceControl").GetComponent<pieceControl>().pieceCount = count;

		//take some health from the player
		control.health-=control.damagePerBoard-control.defense;
		
	}


	private int[,] RotateMatrix(int[,] matrix, int n) {
	    int[,] ret = new int[n, n];

	    for (int i = 0; i < n; ++i) {
	        for (int j = 0; j < n; ++j) {
	            ret[i, j] = matrix[n - j - 1, i];

	            // substitute bomb according to rotation table
	            if (rotationTable.ContainsKey(ret[i,j])) {
	            	ret[i,j] = rotationTable[ret[i,j]];
	            }
	        }
	    }

	    return ret;
	}
}