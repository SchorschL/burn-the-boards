﻿using UnityEngine;
using System.Collections;

public class intro : MonoBehaviour {

	public TextMesh saying;
	public GameObject button;
	private bool done = false;
	
	private global control;
	
	void Start(){
		control = GameObject.Find("Global").GetComponent<global>();
		
		string speak = "In order to feed your family, you have taken a job at an E-waste plant. The fumes will damage your health, and you must manage your funds carefully to survive and work towards a better life.";
		StartCoroutine(saying.GetComponent<wrapText>().loadLine(speak));
	}

	void Update(){
		if(saying.GetComponent<wrapText>().done && !done){
			
			StartCoroutine(addButtons());
			done = true;
		}
	}

	IEnumerator addButtons(){
		
		bool added = false;
		int lines = saying.GetComponent<wrapText>().lines;
		int buttons = 0;
		
		if(!added){
			GameObject newButton;
			newButton = Instantiate(button, new Vector3(3.5f,10-(lines*0.66f)-buttons,0), transform.rotation) as GameObject;
			newButton.GetComponent<button>().instruction = "Begin your journey";
			newButton.GetComponent<button>().message = "newJob";
			newButton.GetComponent<button>().home = this.gameObject;
			buttons+=1;
			yield return new WaitForSeconds(0.3f);
		}
		
	}

	void newJob(){
		CameraFade.StartAlphaFade( Color.white, false, control.cameraFadeDuration, control.cameraFadeDelay, () => { Application.LoadLevel("puzzleIntro1"); } );
	}

}
