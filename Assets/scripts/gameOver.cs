﻿using UnityEngine;
using System.Collections;

public class gameOver : MonoBehaviour {

	public TextMesh saying;
	public GameObject button;
	private bool done = false;


	// Use this for initialization
	void Start () {
		string speak = "You are too weak to go back to work. Game Over";

		StartCoroutine(saying.GetComponent<wrapText>().loadLine(speak));

	}
	
	// Update is called once per frame
	void Update () {
		if(saying.GetComponent<wrapText>().done && !done){
			
			StartCoroutine(addButtons());
			done = true;
		}
	}

	IEnumerator addButtons(){
		
		bool added = false;
		int lines = saying.GetComponent<wrapText>().lines;
		int buttons = 0;

		
		if(!added){
			GameObject newButton;
			newButton = Instantiate(button, new Vector3(3.5f,10-(lines*0.66f)-buttons,0), transform.rotation) as GameObject;
			newButton.GetComponent<button>().instruction = "Go to the menu";
			newButton.GetComponent<button>().message = "toMenu";
			newButton.GetComponent<button>().home = this.gameObject;
			buttons+=1;
			yield return new WaitForSeconds(0.3f);
		}
		
	}

	public void toMenu(){
		Application.LoadLevel("StartScreen");
	}
}
