﻿using UnityEngine;
using System.Collections;

public class shop : MonoBehaviour {

    private global control;
    
    void Start() {
        control = GameObject.Find("Global").GetComponent<global>();
    }


	public void goHome(){
		CameraFade.StartAlphaFade( Color.white, false, control.cameraFadeDuration, control.cameraFadeDelay, () => { Application.LoadLevel("meal"); } );
	}

}
