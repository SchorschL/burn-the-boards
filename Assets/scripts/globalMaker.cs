﻿using UnityEngine;
using System.Collections;

public class globalMaker : MonoBehaviour {

	public GameObject global;

	// Use this for initialization
	void Start () {
	
		if(GameObject.Find("Global")==null){
			GameObject clone = Instantiate(global, Vector3.zero, transform.rotation) as GameObject;
			clone.name = "Global";
		}
	}
}
