﻿using UnityEngine;
using System.Collections;

public class puzzleIntro1 : MonoBehaviour {

	public TextMesh saying;
	public GameObject button;
	private bool done = false;
	
	public string message = "Welcome to my E-Waste plant, I am Mr. Chopra. I will pay you for each board you clear, based upon how well you do. For now, just try to clear all of the components from each board by tracing a line over up to three adjacent pieces.";

	private global control;

	void Start(){
		control = GameObject.Find("Global").GetComponent<global>();
		
		string speak = message;
		StartCoroutine(saying.GetComponent<wrapText>().loadLine(speak));
	}
	
	void Update(){
		if(saying.GetComponent<wrapText>().done && !done){
			StartCoroutine(addButtons());
			done = true;
		}
	}
	
	IEnumerator addButtons(){
		
		bool added = false;
		int lines = saying.GetComponent<wrapText>().lines;
		int buttons = 0;
		
		if(!added){
			GameObject newButton;
			newButton = Instantiate(button, new Vector3(3.5f,10-(lines*0.66f)-buttons,0), transform.rotation) as GameObject;
			newButton.GetComponent<button>().instruction = "Get to work";
			newButton.GetComponent<button>().message = "newJob";
			newButton.GetComponent<button>().home = this.gameObject;
			buttons+=1;
			yield return new WaitForSeconds(0.3f);
		}
		
	}
	
	void newJob(){
		GameObject.Find("magic").GetComponent<AudioSource>().Play();

		control.tutorial=true;
		control.zone=1;

		CameraFade.StartAlphaFade( Color.white, false, control.cameraFadeDuration, control.cameraFadeDelay, () => { Application.LoadLevel("puzzle"); } );
	}
}
