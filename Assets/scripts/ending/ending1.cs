﻿using UnityEngine;
using System.Collections;

public class ending1 : MonoBehaviour {

	public TextMesh saying;
	public GameObject button;
	private bool done = false;

	public string endingText;

	private global control;
	
	void Start(){
		control = GameObject.Find("Global").GetComponent<global>();
		

		string speak = endingText;
		StartCoroutine(saying.GetComponent<wrapText>().loadLine(speak));
	}
	
	void Update(){
		if(saying.GetComponent<wrapText>().done && !done){
			
			StartCoroutine(addButtons());
			done = true;
		}
	}
	
	IEnumerator addButtons(){
		
		bool added = false;
		int lines = saying.GetComponent<wrapText>().lines;
		int buttons = 0;
		
		if(!added){
			GameObject newButton;
			newButton = Instantiate(button, new Vector3(3.5f,10-(lines*0.66f)-buttons,0), transform.rotation) as GameObject;
			newButton.GetComponent<button>().instruction = "End your journey";
			newButton.GetComponent<button>().message = "newJob";
			newButton.GetComponent<button>().home = this.gameObject;
			buttons+=1;
			yield return new WaitForSeconds(0.3f);
		}
		
	}
	
	void newJob(){
		CameraFade.StartAlphaFade( Color.white, false, control.cameraFadeDuration, control.cameraFadeDelay, () => { Application.LoadLevel("startScreen"); } );
	}
}
