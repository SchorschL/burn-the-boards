﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class global : MonoBehaviour {

	public string levelToLoad;
	public int boardsToday = 0;
	public int boardsWon = 0;

	public bool debug = false;
	public bool tutorial = false;

	public int money = 0;
	public int health = 0;
	public int food = 5;
	public int shelter = 100;
	public int wifeHealth = 0;

	public int[] events = new int[99];

	public int stars = 0;
	public int totalStars = 0;

	public int rank = 1;

	public int boardsPerDay = 5;
	public int damagePerBoard = 5;
	public int defense = 0;
	public int healPerDay = 0;

	public int[] itemStore = new int[10];//storing the status of single purchase items to avoid multiple purchases

	public List<string> playedBoards = new List<string>();

	public int[] rankUp = new int[13] //the number of stars required to rank up
	{
		15,30,45,60,75,90,105,120,135,150,165,180,195
	};

	public int days = 0;//just a day counter

	public int wifeTask = 0;//record the task for random events [see the home script for reference]

	public Sprite[] healthSprites;

	public int level = 0;
	public int zone = 0;

	public int maxSwipe = 3;//the length of the max swipe
	public bool dead = false;

	public float cameraFadeDelay = 0f;
	public float cameraFadeDuration = 1f;

	//update the health bar to show the value
	void healthBar(){

		int sprite = health/10;

		//capping for the array
		if(sprite>9){
			sprite=9;
		}

		//display the sprite
		GameObject.Find("healthBar").GetComponent<SpriteRenderer>().sprite = healthSprites[sprite];
	}


	public List<string> PlayedBoardsInBatch(int batch) {
		List<string> boards = new List<string>();
		foreach (string board in playedBoards) {
			if (board.Split('_')[0] == batch.ToString()) {
				boards.Add(board);
			}
		}
		return boards;
	}


	void Update(){

		if(health<0 && !dead){
			dead = true;
			Application.LoadLevel("GameOver");
		}

		if(GameObject.Find("healthBar")!=null){
			healthBar();
		}

		if(GameObject.Find("Whealth")!=null){
			GameObject.Find("Whealth").GetComponent<TextMesh>().text = wifeHealth + "HP - Wife";
		}

		if(GameObject.Find("shelter")!=null){
			GameObject.Find("shelter").GetComponent<TextMesh>().text = shelter + " - Shelter";
		}

		if(GameObject.Find("food")!=null){
			GameObject.Find("food").GetComponent<TextMesh>().text = food + "FOOD";
		}

		if(GameObject.Find("money")!=null){
			GameObject.Find("money").GetComponent<TextMesh>().text = money + "R";
		}

		//limits
		if(health>100){
			health=100;
		}
		if(health<0){
			health=0;
		}
		if(wifeHealth>100){
			wifeHealth=100;
		}
	}

	void Awake() {
		DontDestroyOnLoad(transform.gameObject);
	}

}
