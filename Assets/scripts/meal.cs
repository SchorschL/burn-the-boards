﻿using UnityEngine;
using System.Collections;

public class meal : MonoBehaviour {

	public TextMesh saying;
	public GameObject button;
	private bool done = false;


	private string[] dailySpeech = new string[15]
	{
		"Arun, I'm so happy you found work! Now we can have a place for ourselves. ",
		"The power keeps on shutting down. But at least we have electricity for most of the day! ",
		"I met some people today, they came from the village just North of ours! We are really lucky, they have to collect old paper for a living. And perhaps one day, we have enough money to have a little shop or you could go to college. ",
		"EVENT Your brother called. He wants us to lend him money. Let's hope his business idea will work out this time.",
		"Arun, if you keep up your work, you might be able to save enough money to look for a better job! I keep my ears open, maybe we could take over a street kitchen.",
		"Look, I bought this new sari!",
		"EVENT CONCLUSION Your brother called.",
		"Arun, I saw it on the news, that these materials at your work are really toxic! Maybe Mister Chopra has something against the fumes.",
		"Please wash your hands, Arun. I don't want you to pick up the baby with all this toxic filth on your fingers.",
		"There was another news item on toxic polution. Please, be careful at work, there is mercury, PVC and all sorts of things that could cause cancer in those fumes!”",
		"EVENT My parents called. The last harvest failed. They really need our help.",
		"Please do something about the coughing, Arun. It’s really annoying, I can’t sleep.",
		"Do you think you got get us a new television from work? The old one is broken again.",
		"EVENT My parents just called.",
		"Your rash doesn't look to good today, Arun. Please go and see a doctor."
	};

	private string[] genericSpeech = new string[9]
	{
		"How is your headache today, Arun? ",
		"My new gold necklace is gone. Do you know where it is? ",
		"What is that on your finger? ",
		"Mister Choprak should really do something about those toxic fumes! It’s so irresponsible. ",
		"The rash on your face is getting worse. ",
		"Please see a doctor soon, Arun. ",
		"It’s really not fair, that we are getting poisoned, please, find a new job.",
		"The lady of the house is really not nice to me, I should find a new job soon. ",
		"I hope one day we can send our children to a proper school. "

	};

	void Start(){
		global control = GameObject.Find("Global").GetComponent<global>();

		string speak = "";

		//if it is still before day 15, make the wife say a set storyline thing
		if(control.days<15){
			speak+=dailySpeech[control.days];
		}
		//generic later
		if(control.days>=15){
			speak+=genericSpeech[Random.Range(0,genericSpeech.Length)];
		}

		int type3 = Random.Range(0,4);
		//try and give an update on the aunt
		if(type3==1){
			if(control.events[5]==1){
				int type = Random.Range(0,5);
				if(type==0){
					speak += "My aunt is much better now. She sent some food as a thanks for my visit.\n";
					control.food+=1;
				}
				if(type==1){
					speak += "My aunt died earlier today. She left what little she had to us. Let's use it wisely.\n";
					control.money+=50;
				}
				control.events[5]=2;
			}
		}

		int type2 = Random.Range(0,5);
		if(type2==0){
			speak += "Welcome home husband.\n";
		}
		if(type2==1){
			speak += "I'm glad you are home.\n";
		}
		if(type2==2){
			speak += "How was your day?\n";
		}
		if(type2==3){
			speak += "There you are! Welcome back.\n";
		}
		if(type2==4){
			speak += "Ah, I didn't hear you come in!\n";
		}

		//say something related to what was just done
		if(control.wifeTask==0){

			int type = Random.Range(0,5);
			if(type==0){
				speak += "I had a restful day today.\n";
				control.wifeHealth+=20;
			}
			if(type==1){
				speak += "I met an old friend today.\n";
			}
			if(type==2){
				speak += "There was a festival today, I bought back some spare food.\n";
				control.food+=1;
			}
			if(type==3){
				speak += "Did you hear about the cricket? It was exciting!\n";
			}
			if(type==4){
				speak += "I looked after the neighbors children this morning.\nI would like kids some day.\n";
			}
		}

		if(control.wifeTask==1){
			int type = Random.Range(0,3);
			if(type==0){
				speak += "I got us a little food from the market today.\n";
			}
			if(type==1){
				speak += "I got all the food that I could afford, I don't think that it will go far...\n";
			}
			if(type==2){
				speak += "Deepack had too much rice delivered today, so he let me have some for free!\n";
				control.food+=1;
			}
		}

		if(control.wifeTask==2){
			int type = Random.Range(0,3);
			if(type==0){
				speak += "I got us plenty of food from the market today.\n";
			}
			if(type==1){
				speak += "I got us some food earlier, it should last a few days.\n";
			}
			if(type==2){
				speak += "I haggled with Mr Patel, and got some onions for free!\n";
				control.food+=1;
			}
		}
		if(control.wifeTask==3){
			int type = Random.Range(0,3);
			if(type==0){
				speak += "I fixed a leak in the roof earlier\n";
			}
			if(type==1){
				speak += "I found a metal sheet and managed to patch up the front wall.\n";
			}
			if(type==2){
				speak += "My brother called by and helped me to fix the roof. He gave us some spare materials!\n";
				control.shelter+=10;
			}
		}
		if(control.wifeTask==4){
			int type = Random.Range(0,3);
			if(type==0){
				speak += "The builders have been, the repairs went fine.\n";
			}
			if(type==1){
				speak += "The builders have secured the front and back walls, so the wind should bother us less now.\n";
			}
			if(type==2){
				speak += "The builder that arrived said he knows you! He gave us a some money off the repairs.\n";
				control.money+=10;
			}
		}

		if(control.wifeTask==11){
			int type = Random.Range(0,3);
			if(type==0){
				speak += "I had a wonderful time wiht my brother, thank you.\n";
			}
			if(type==1){
				speak += "My brother bought us some vegetables from our parents.[+4FOOD]\n";
				control.food+=4;
			}
			if(type==2){
				speak += "My brother came earlier, he helped to repair the roof.[+10 shelter]\n";
				control.shelter+=10;
			}
		}
		if(control.wifeTask==12){
			int type = Random.Range(0,3);
			if(type==0){
				speak += "My aunt seems to be doing very well.\n";
			}
			if(type==1){
				speak += "I visited my aunt today, she isn't doing too good.\n";
			}
			if(type==2){
				speak += "My aunt has problems breathing from fumes. I hope that doesn't happen to you.\n";
			}
		}

		speak += "What would you like to eat?";

		print(speak);
		StartCoroutine(saying.GetComponent<wrapText>().loadLine(speak));

		control.wifeTask=0;
	}

	void Update(){
		if(saying.GetComponent<wrapText>().done && !done){
			
			StartCoroutine(addButtons());
			done = true;
		}
	}

	IEnumerator addButtons(){
		
		bool added = false;
		int lines = saying.GetComponent<wrapText>().lines;
		int buttons = 0;
		
		global control = GameObject.Find("Global").GetComponent<global>();
		
		if(!added){
			GameObject newButton;
			newButton = Instantiate(button, new Vector3(3.5f,10-(lines*0.66f)-buttons,0), transform.rotation) as GameObject;
			newButton.GetComponent<button>().instruction = "We can't eat today [-25hp]";
			newButton.GetComponent<button>().message = "noFood";
			newButton.GetComponent<button>().home = this.gameObject;
			buttons+=1;
			yield return new WaitForSeconds(0.3f);
		}
		if(!added && control.food>=2){
			GameObject newButton;
			newButton = Instantiate(button, new Vector3(3.5f,10-(lines*0.66f)-buttons,0), transform.rotation) as GameObject;
			newButton.GetComponent<button>().instruction = "Lets eat a small meal [2 food]";
			newButton.GetComponent<button>().message = "smallMeal";
			newButton.GetComponent<button>().home = this.gameObject;
			buttons+=1;
			yield return new WaitForSeconds(0.3f);
		}
		if(!added && control.food>=4){
			GameObject newButton;
			newButton = Instantiate(button, new Vector3(3.5f,10-(lines*0.66f)-buttons,0), transform.rotation) as GameObject;
			newButton.GetComponent<button>().instruction = "Lets eat plenty [4 food, +25hp]";
			newButton.GetComponent<button>().message = "bigMeal";
			newButton.GetComponent<button>().home = this.gameObject;
			buttons+=1;
			yield return new WaitForSeconds(0.3f);
		}
		if(!added && control.money>-40){
			GameObject newButton;
			newButton = Instantiate(button, new Vector3(3.5f,10-(lines*0.66f)-buttons,0), transform.rotation) as GameObject;
			newButton.GetComponent<button>().instruction = "Lets go out for some food [-40R]";
			newButton.GetComponent<button>().message = "eatOut";
			newButton.GetComponent<button>().home = this.gameObject;
			buttons+=1;
			yield return new WaitForSeconds(0.3f);
		}
		
	}

	public void noFood(){
		global control = GameObject.Find("Global").GetComponent<global>();
		control.wifeHealth-=25;
		control.health-=25;
		Application.LoadLevel("home");
	}
	public void smallMeal(){
		global control = GameObject.Find("Global").GetComponent<global>();
		control.food-=2;
		Application.LoadLevel("home");
	}
	public void bigMeal(){
		global control = GameObject.Find("Global").GetComponent<global>();
		control.food-=4;
		control.wifeHealth+=25;
		control.health+=25;
		Application.LoadLevel("home");

	}
	public void eatOut(){
		global control = GameObject.Find("Global").GetComponent<global>();
		control.money-=40;
		Application.LoadLevel("home");
	}

}
