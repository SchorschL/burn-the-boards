﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class pieceControl : MonoBehaviour {

	public int score = 0;
	public int goal = 0;

	public int multiply = 0;
	public int multiplyB = 0;

	public int count = 0;
	public GameObject lastTile;

	public int pieceCount = 0;
	public int valuePieceCount = 0;
	public int pieceDestroyed = 0;
	public int valuePieceDestroyed = 0;

	public int silver = 0;
	public int gold = 0;
	public int rank = 0;

	public GameObject[] stars;

	public bool complete = false;
	public bool won = false;

	public List<GameObject> tiles = new List<GameObject>();

	public GameObject skyline;
	public Sprite[] skys;

	public GameObject endScreen;

	public Sprite[] progressSprites;
	public GameObject progressBar;
	int sprite = 0;

	public int maxSwipe = 3;

	public bool swiping = false;
	public bool canSwipe = true;

	public int added = 0;

	// game analytics
	private global control;
	private float startTime;
	private bool firstSwipe = true;

	public bool canSwipe2 = true;

	public bool noAnalytics = false;

	void Awake(){
		skyline.GetComponent<SpriteRenderer>().sprite = skys[GameObject.Find("Global").GetComponent<global>().boardsToday];
		maxSwipe = GameObject.Find("Global").GetComponent<global>().maxSwipe;

		// analytics
		control = GameObject.Find("Global").GetComponent<global>();
		startTime = Time.time;
	}


	void progress(){

		//space out version
		//first get the goal to silver
		if(score<silver){
			float prog = (((float)silver-(float)score)/(float)silver)*5.0f;
			sprite = 5-(int)prog;
			//show the correct sprite on the bar
			progressBar.GetComponent<SpriteRenderer>().sprite = progressSprites[sprite];

		}
		if(score>=silver){
			float newgoal = (float)silver-(float)gold;
			float newScore = (float)silver - (float)score;
			float prog = ((newgoal-newScore)/newgoal)*5.0f;
			sprite = (5-(int)prog)+5;
			if(sprite>=10){
				sprite=9;
			}
			//show the correct sprite on the bar
			progressBar.GetComponent<SpriteRenderer>().sprite = progressSprites[sprite];
		}

		//show the stars when needed
		if(score>=silver){
			GameObject.Find("silverComplete").GetComponent<Renderer>().enabled=true;
		}
		if(score>=gold){
			GameObject.Find("goldComplete").GetComponent<Renderer>().enabled=true;
		}

	}

	void Update(){

		//showing the multiplies
		if(multiplyB>0){
			GameObject.Find("multi").GetComponent<TextMesh>().text = "x"+multiplyB;
		}
		else{
			GameObject.Find("multi").GetComponent<TextMesh>().text = "";
		}

		//swiping control
		if (Input.GetMouseButtonDown(0) && count<maxSwipe && canSwipe){
			//empty out the sum array
			GameObject.Find("sum").GetComponent<TextMesh>().text = "";
			GameObject.Find("sum").transform.position = new Vector3(GameObject.Find("sum").transform.position.x,GameObject.Find("sum").transform.position.y,2);
			


			//make a new gameobject to hold the trace
			GameObject tracer = new GameObject();
			tracer.name = "tracer";

			swiping = true;
			canSwipe = false;

			// analytics: log time of first swipe
			if (firstSwipe) {
				GA.API.Design.NewEvent("Work:FirstSwipe:" + control.zone + "_" + control.level + ":" + (int)(Time.time-startTime));
				GA.API.Design.NewEvent("Work:FirstSwipe:" + control.zone + "_" + control.level, Time.time-startTime);
				firstSwipe = false;
			}
		}
		if (Input.GetMouseButtonUp(0)){

			canSwipe = true;
			swiping =false;

			if(tiles.Count>0){
				StartCoroutine(checker());
			}
		}


		//working out the current progress to display on the bar
		progress();

		if (Input.GetKeyUp("space") && !complete){
			//Reset();
		}
		if (Input.GetKeyUp("space") && complete){
			
			GameObject.Find("Global").GetComponent<global>().boardsToday+=1;
			if(won){
				GameObject.Find("Global").GetComponent<global>().boardsWon+=1;
			}

			//load another puzzle if needed
			if(GameObject.Find("Global").GetComponent<global>().boardsToday<5){

				CameraFade.StartAlphaFade( Color.white, false, control.cameraFadeDuration, control.cameraFadeDelay, () => { Application.LoadLevel("puzzle"); } );
			}
			//otherwise, go to the shop
			if(GameObject.Find("Global").GetComponent<global>().boardsToday==5){
				Application.LoadLevel("shopScreen");
				CameraFade.StartAlphaFade( Color.white, false, control.cameraFadeDuration, control.cameraFadeDelay, () => { Application.LoadLevel("shopScreen"); } );
			}
		}

		if(valuePieceDestroyed>=valuePieceCount && valuePieceCount<900 && canSwipe && canSwipe2){
			Debug.Log("valuePieceDestroyed: " + valuePieceDestroyed);
			Debug.Log("valuePieceCount: " + valuePieceCount);
			pieceCount=999;
			valuePieceCount = 999;
			GameObject.Find("magic").GetComponent<AudioSource>().Play();
			//bloop
			endScreen.GetComponent<Renderer>().enabled=true;
			endScreen.GetComponent<Animation>().Play();

		
			if (!noAnalytics) {
				Debug.Log("submit level end analytics");
				// log level complete with time
				GA.API.Design.NewEvent("Work:Time:" + control.zone + "_" + control.level + ":" + (int)(Time.time-startTime));
				GA.API.Design.NewEvent("Work:Time:" + control.zone + "_" + control.level, Time.time-startTime);

				// analytics
				GA.API.Design.NewEvent("Work:Coins:" + control.zone + "_" + control.level + ":" + score);
				GA.API.Design.NewEvent("Work:Coins:" + control.zone + "_" + control.level, score);
			}

			if(score<silver){
				rank = 0;
				complete=true;
				stars[0].GetComponent<Renderer>().enabled=true;
				GameObject.Find("Global").GetComponent<global>().stars+=1;
				if (!noAnalytics) {
					// analytics
					GA.API.Design.NewEvent("Work:Stars:" + control.zone + "_" + control.level + ":" + 1);
					GA.API.Design.NewEvent("Work:Stars:" + control.zone + "_" + control.level, 1);
				}
			}
			if(score>=silver && score<gold){
				rank = 1;
				complete=true;
				stars[0].GetComponent<Renderer>().enabled=true;
				stars[1].GetComponent<Renderer>().enabled=true;
				GameObject.Find("Global").GetComponent<global>().stars+=2;
				if (!noAnalytics) {
					// analytics
					GA.API.Design.NewEvent("Work:Stars:" + control.zone + "_" + control.level + ":" + 2);
					GA.API.Design.NewEvent("Work:Stars:" + control.zone + "_" + control.level, 2);
				}
			}
			if(score>=gold){
				rank = 2;
				complete=true;
				stars[0].GetComponent<Renderer>().enabled=true;
				stars[1].GetComponent<Renderer>().enabled=true;
				stars[2].GetComponent<Renderer>().enabled=true;
				GameObject.Find("Global").GetComponent<global>().stars+=3;
				if (!noAnalytics) {
					// analytics
					GA.API.Design.NewEvent("Work:Stars:" + control.zone + "_" + control.level + ":" + 3);
					GA.API.Design.NewEvent("Work:Stars:" + control.zone + "_" + control.level, 3);
				}
			}
			noAnalytics = false;
		}
	}

	public IEnumerator checker(){
		if(canSwipe2){
			canSwipe2=false;

			GameObject.Find("whoosh").GetComponent<AudioSource>().Play();
	 		swiping = false;

			foreach(GameObject tile in tiles){
				if(tile!=null){
					if(tile.GetComponent<pieceInteract>().killed==false){
						tile.GetComponent<pieceInteract>().kill();
					}
					else{
						break;
					}
				}
				
				yield return new WaitForSeconds(0.2f);
			}

			//make the ones that werent just killed usable again
			foreach(GameObject tile in tiles){
				if(tile!=null){
					if(tile.GetComponent<pieceInteract>().killed==false || tile.GetComponent<pieceInteract>().voidPiece){
						tile.GetComponent<pieceInteract>().used=false;
						tile.GetComponent<Renderer>().material.color=Color.white;
					}
				}
			}

			print("total" + added + "  score -" + score);
			StartCoroutine(endScore());
		}

	}

	IEnumerator endScore(){
		canSwipe2=false;

		//compress the score into the middle
		Vector3 newscale = GameObject.Find("sum").transform.localScale;

		GameObject.Find("sum").transform.position = new Vector3(GameObject.Find("sum").transform.position.x,GameObject.Find("sum").transform.position.y,-2);

		yield return new WaitForSeconds(0.2f);

		while(GameObject.Find("sum").transform.localScale.x>0){
			yield return new WaitForSeconds(0.01f);
			Vector3 newscale2 = new Vector3(newscale.x-0.01f,newscale.y,newscale.z);
			newscale = newscale2;
			GameObject.Find("sum").transform.localScale= newscale2;
		}


		GameObject.Find("sum").transform.localScale = new Vector3(0.2f,0.2f,0.2f);

		int subScore = added;
		GameObject.Find("sum").GetComponent<TextMesh>().text= subScore.ToString();

		yield return new WaitForSeconds(1.0f);

		//count down the added score
		while(subScore>0){
			score+=1;
			subScore-=1;
			GameObject.Find("sum").GetComponent<TextMesh>().text= subScore.ToString();
			yield return new WaitForSeconds(0.05f);
		}
		GameObject.Find("sum").GetComponent<TextMesh>().text="";
		added = 0;
		//reset the values where needed
		
		lastTile = null;
		Destroy(GameObject.Find("tracer"));
		count=0;
		multiply=0;
		multiplyB=0;
		tiles.Clear();
		canSwipe2=true;

	}

	void Reset(){
		//reset the values where needed
		foreach(GameObject tile in tiles){
			tile.GetComponent<Renderer>().material.color=Color.white;
		}
		tiles.Clear();
		count=0;

		multiply=0;
		multiplyB=0;

	}

	void OnGUI() {
		global control = GameObject.Find("Global").GetComponent<global>();

		if(control.debug){
			if (GUI.Button(new Rect(0, 0, 150, 30), "Complete all [DEBUG]")){
				GameObject.Find("Global").GetComponent<global>().boardsToday=5;
				GameObject.Find("Global").GetComponent<global>().boardsWon=5;
				GameObject.Find("Global").GetComponent<global>().health-=25;
				Application.LoadLevel("shopScreen");
			}
		}
	}
}
