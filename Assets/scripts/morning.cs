﻿using UnityEngine;
using System.Collections;

public class morning : MonoBehaviour {

	//decide if you want to go to work or not, plus bonuses from the night before
	public TextMesh saying;
	public GameObject button;

	private bool done = false;

	private global control;

	// Use this for initialization
	void Start () {
	
		control = GameObject.Find("Global").GetComponent<global>();

		//add the health on top
		control.health+=control.healPerDay;

		control.days+=1;

		string message = "";
		
		int weather = Random.Range(0,3);
		if(weather==0){
			message+="The night was cool and calm. ";

			if(control.shelter>=80){
				message+= "You slept well and are ready for a new day [+20hp]";
				control.health+=20;
			}
			if(control.shelter<80 && control.shelter>50){
				message+= "You and your wife slep comfortably, and wake for a new day";
			}
			if(control.shelter>=50 && control.shelter<20){
				message+= "You and your wife didn't get much sleep last night...";
				control.health-=Random.Range(0,20);
			}
			if(control.shelter<20){
				message+= "Your home is in ruins, you go not sleep at all";
				control.health-=Random.Range(20,40);
			}
		}
		if(weather==1){
			message+="It was quite windy last night, part of the roof is coming loose. [-10shelter]\n";
			control.shelter-=10;

			if(control.shelter>=80){
				message+= "You slept well and are ready for a new day [+20hp]";
				control.health+=20;
			}
			if(control.shelter<80 && control.shelter>50){
				message+= "You and your wife slep comfortably, and wake for a new day";
			}
			if(control.shelter>=50 && control.shelter<20){
				message+= "You and your wife didn't get much sleep last night...";
				control.health-=Random.Range(0,20);
			}
			if(control.shelter<20){
				message+= "Your home is in ruins, you go not sleep at all";
				control.health-=Random.Range(20,40);
			}
		}
		if(weather==2){
			if(control.shelter>=50){
				message+="It was stormy last night, but your shelter kept you warm and dry\n";
			}
			if(control.shelter<50){
				message+="It was stormy last night, and your leaky roof kept you up all night. [-10hp]\n";
				control.health-=10;
			}
		}

		StartCoroutine(saying.GetComponent<wrapText>().loadLine(message));

	
	}

	void Update(){

		//add the buttons when the message has finished typing
		if(saying.GetComponent<wrapText>().done && !done){

			StartCoroutine(addButtons());
			done = true;
		}

	}

	IEnumerator addButtons(){

		bool added = false;
		int lines = saying.GetComponent<wrapText>().lines;
		int buttons = 0;

		if(!added){
			GameObject newButton;
			newButton = Instantiate(button, new Vector3(3.5f,10-(lines*0.66f)-buttons,0), transform.rotation) as GameObject;
			newButton.GetComponent<button>().instruction = "Go To Work";
			newButton.GetComponent<button>().message = "goWork";
			newButton.GetComponent<button>().home = this.gameObject;
			buttons+=1;
			yield return new WaitForSeconds(0.3f);
		}
		if(!added){
			GameObject newButton;
			newButton = Instantiate(button, new Vector3(3.5f,10-(lines*0.66f)-buttons,0), transform.rotation) as GameObject;
			newButton.GetComponent<button>().instruction = "I need to rest today [+40hp]";
			newButton.GetComponent<button>().message = "rest";
			newButton.GetComponent<button>().home = this.gameObject;
			buttons+=1;
			yield return new WaitForSeconds(0.3f);
		}

		//adding the end game buttons when the right number of days have been played

		if(control.days>=4){

			if(!added){
				GameObject newButton;
				newButton = Instantiate(button, new Vector3(3.5f,10-(lines*0.66f)-buttons,0), transform.rotation) as GameObject;
				newButton.GetComponent<button>().instruction = "Go to college [1000r]";
				newButton.GetComponent<button>().message = "End";
				newButton.GetComponent<button>().home = this.gameObject;
				buttons+=1;
				yield return new WaitForSeconds(0.3f);
			}

			if(!added){
				GameObject newButton;
				newButton = Instantiate(button, new Vector3(3.5f,10-(lines*0.66f)-buttons,0), transform.rotation) as GameObject;
				newButton.GetComponent<button>().instruction = "Start a street food stall [1500r]";
				newButton.GetComponent<button>().message = "End2";
				newButton.GetComponent<button>().home = this.gameObject;
				buttons+=1;
				yield return new WaitForSeconds(0.3f);
			}

			if(!added){
				GameObject newButton;
				newButton = Instantiate(button, new Vector3(3.5f,10-(lines*0.66f)-buttons,0), transform.rotation) as GameObject;
				newButton.GetComponent<button>().instruction = "Open an electronics store [2000r]";
				newButton.GetComponent<button>().message = "End3";
				newButton.GetComponent<button>().home = this.gameObject;
				buttons+=1;
				yield return new WaitForSeconds(0.3f);
			}

		}

	}

	void goWork(){


		//first check for a rank up, if it is needed then
		if(control.rank < control.rankUp.Length+1 && control.totalStars>control.rankUp[control.rank-1]){
			//increase the rank
			control.rank+=1;
			control.tutorial = true;
			//load the tutorial intro for that zone
			if(control.rank==2){
				Application.LoadLevel("puzzleIntro2");
			}
			if(control.rank==3){
				Application.LoadLevel("puzzleIntro3");
			}
			if(control.rank==4){
				Application.LoadLevel("puzzleIntro4");
			}
			if(control.rank==5){
				Application.LoadLevel("puzzleIntro5");
			}
			if(control.rank==6){
				Application.LoadLevel("puzzleIntro6");
			}
			if(control.rank==7){
				Application.LoadLevel("puzzleIntro7");
			}
			else{
				CameraFade.StartAlphaFade( Color.white, false, control.cameraFadeDuration, control.cameraFadeDelay, () => { Application.LoadLevel("puzzle"); } );
			}
		}
		else{
			CameraFade.StartAlphaFade( Color.white, false, control.cameraFadeDuration, control.cameraFadeDelay, () => { Application.LoadLevel("puzzle"); } );
		}

	}
	void rest(){
		control.health+=40;
		CameraFade.StartAlphaFade( Color.white, false, control.cameraFadeDuration, control.cameraFadeDelay, () => { Application.LoadLevel("meal"); } );
	}
	void helpWife(){
		control.wifeHealth+=40;
		CameraFade.StartAlphaFade( Color.white, false, control.cameraFadeDuration, control.cameraFadeDelay, () => { Application.LoadLevel("meal"); } );
	}

	//ending scenes
	void End(){
		if(control.money>1000){
			CameraFade.StartAlphaFade( Color.white, false, control.cameraFadeDuration, control.cameraFadeDelay, () => { Application.LoadLevel("end"); } );
		}
	}
	void End2(){
		if(control.money>1500){
			CameraFade.StartAlphaFade( Color.white, false, control.cameraFadeDuration, control.cameraFadeDelay, () => { Application.LoadLevel("end2"); } );
		}
	}
	void End3(){
		if(control.money>2000){
			CameraFade.StartAlphaFade( Color.white, false, control.cameraFadeDuration, control.cameraFadeDelay, () => { Application.LoadLevel("end3"); } );
		}
	}


}