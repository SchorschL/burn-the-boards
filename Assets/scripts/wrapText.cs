﻿using UnityEngine;
using System.Collections;

public class wrapText : MonoBehaviour
{
	public TextMesh textMesh;
	
	public int maxLineChars = 35;
	private int lastMaxLineChars = 35;
	
	string wrappedText = "";
	public string finalLine = "";

	public int lines = 0;
	public bool done = false;

	private bool jumpSpeech = false;

	void Start(){
		textMesh = this.gameObject.GetComponent<TextMesh>();

	}

	public void JumpSpeech() {
		this.jumpSpeech = true;
	}

	public IEnumerator loadLine(string line){
		//display the line with a typewrite effect
		string displayLine = "";
		for(int i2 = 0; i2 < line.Length; i2++)
		{
			displayLine += line.Substring(i2,1);
			textMesh.text = displayLine;
			lines = 1;

			if(textMesh.text != wrappedText || maxLineChars != lastMaxLineChars)
			{
				int charCount = 0;
				wrappedText = "";
				string line2 = "";
				
				char[] separators = new char[]{' ','\n','\t'};
				string[] words = textMesh.text.Split(separators);
				
				for(int i = 0; i < words.Length; i++)
				{
					string word = words[i].Trim();


					if(i == 0)
					{
						line2 = word;
						charCount = word.Length;
					}
					else
					{
						if((charCount + (charCount > 0 ? 1 : 0) + word.Length) <= maxLineChars)
						{
							if(charCount > 0)
							{
								line2 += ' ';
								charCount += 1;
							}
							
							line2 += word;
							charCount += word.Length;
						}
						else
						{
							if(wrappedText.Length > 0){
								lines+=1;
								wrappedText += '\n';
							}
							
							wrappedText += line2;
							
							line2 = word;
							charCount = word.Length;
						}
					}
				}
				
				if(charCount > 0)
				{
					if(wrappedText.Length > 0){
						wrappedText += '\n';
						lines+=1;
					}
					
					wrappedText += line2;
				}
				
				textMesh.text = wrappedText;
				lastMaxLineChars = maxLineChars;
			}
			if (jumpSpeech) {
				// yield return new WaitForSeconds(0);
				textMesh.text = wrappedText;
			} else {
				if(wrappedText[wrappedText.Length-1].ToString()=="." || wrappedText[wrappedText.Length-1].ToString()=="!"
				   || wrappedText[wrappedText.Length-1].ToString()=="?"){
					yield return new WaitForSeconds(0.2f);
				}
				else{
					GameObject.Find("click-2").GetComponent<AudioSource>().Play();
					yield return new WaitForSeconds(0.03f);
				}
			}
		}

		//ending it
		done = true;
		
	}

}