﻿using UnityEngine;
using System.Collections;

public class tempMenu : MonoBehaviour {

	public int levels = 10;
	public int zones = 14;

	void OnGUI() {

		for(int x = 0; x < zones; x++)
		{
			for(int y = 0; y < levels; y++)
			{

				if (GUI.Button(ResizeGUI(new Rect(37*y,30*x, 37, 30)), (x+1) + "_" + (y+1))){

					GameObject.Find("Global").GetComponent<global>().maxSwipe = 2;

					if(x>=6){
						GameObject.Find("Global").GetComponent<global>().maxSwipe = 3;
					}
					if(x>=8){
						GameObject.Find("Global").GetComponent<global>().maxSwipe = 4;
					}

					GameObject.Find("Global").GetComponent<global>().zone = x+1;
					GameObject.Find("Global").GetComponent<global>().level = y+1;
					Application.LoadLevel("puzzle");
				}
			}
		}
	}

	Rect ResizeGUI(Rect _rect)
	{
		float FilScreenWidth = _rect.width / 375;
		float rectWidth = FilScreenWidth * Screen.width;
		float FilScreenHeight = _rect.height / 667;
		float rectHeight = FilScreenHeight * Screen.height;
		float rectX = (_rect.x / 375) * Screen.width;
		float rectY = (_rect.y / 667) * Screen.height;
		
		return new Rect(rectX,rectY,rectWidth,rectHeight);
	}
}
