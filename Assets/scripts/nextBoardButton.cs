﻿using UnityEngine;
using System.Collections;

public class nextBoardButton : MonoBehaviour {

	private global control;

	void Start() {
		control = GameObject.Find("Global").GetComponent<global>();
	}

	void OnMouseDown(){

		//CameraFade.StartAlphaFade( Color.white, false, 1f, 1f, () => { Application.LoadLevel("tempMenu"); } );

		//removing this for the temp menu
		control.boardsToday+=1;
		//load another puzzle if needed
		if(control.boardsToday<5){
			control.tutorial=false;
			CameraFade.StartAlphaFade( Color.white, false, control.cameraFadeDuration, control.cameraFadeDelay, () => { Application.LoadLevel("puzzle"); } );
		}
		//otherwise, go to the shop
		if(control.boardsToday==5){
			control.tutorial=false;
			CameraFade.StartAlphaFade( Color.white, false, control.cameraFadeDuration, control.cameraFadeDelay, () => { Application.LoadLevel("shopScreen"); } );
		}
	}
}
