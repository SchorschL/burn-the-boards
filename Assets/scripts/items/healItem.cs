﻿using UnityEngine;
using System.Collections;

public class healItem : MonoBehaviour {

	//heal by a set amount then destroy the object
	public int health = 0;

	// Use this for initialization
	void Start () {
	
		global control = GameObject.Find("Global").GetComponent<global>();
		control.health += health;
		Destroy(gameObject);

	}

}
