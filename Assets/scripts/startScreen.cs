﻿using UnityEngine;
using System.Collections;

public class startScreen : MonoBehaviour {

    private global control;

    void Start() {
        control = GameObject.Find("Global").GetComponent<global>();
    }

	void OnMouseDown(){
        if (this.gameObject.name == "newGameButton") {
            GameObject.Find("soundlogo4").GetComponent<AudioSource>().Play();
            CameraFade.StartAlphaFade( Color.white, false, control.cameraFadeDuration, control.cameraFadeDelay, () => { Application.LoadLevel("intro"); } );
        }
        if (this.gameObject.name == "loadGameButton") {
            // here would the load game funcitonality go
        }
    }

}
