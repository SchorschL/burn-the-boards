﻿using UnityEngine;
using System.Collections;

public class summary : MonoBehaviour {

	public TextMesh saying;
	public GameObject button;
	private bool done = false;

	private global control;

	// Use this for initialization
	void Start () {

		control = GameObject.Find("Global").GetComponent<global>();
	
		string message = "";

		message += "You got " + control.stars + " stars today.\nHere is your payment of " + (control.stars*3) +
			 " R. See you soon! ";

		if(control.totalStars>=15){
			control.rank+=1;
			control.totalStars-=15;

			message += "I am very impressed with your work so far. From tomorrow I will give you more expensive boards to burn!";
		}
		else{
			if(control.money>150){
				message+= "I see you have saved quite a few rupees? You should visit my shop!";
			}
			else if(control.health<30){
				message+= "You don't look too good. Make sure to buy some protection! I have very reasonable prices";
			}
		}

		StartCoroutine(saying.GetComponent<wrapText>().loadLine(message));

		//check to see how many boards were completed, and give a reward based upon that
		control.money += control.stars*3;
		control.boardsWon = 0;
		control.boardsToday = 0;
		control.totalStars +=control.stars;
		control.stars = 0;

	}

	void Update(){
		if(saying.GetComponent<wrapText>().done && !done){
			
			StartCoroutine(addButtons());
			done = true;
		}
	}

	IEnumerator addButtons(){
		
		bool added = false;
		int lines = saying.GetComponent<wrapText>().lines;
		int buttons = 0;

		global control = GameObject.Find("Global").GetComponent<global>();

		if(!added && control.money>60){
			GameObject newButton;
			newButton = Instantiate(button, new Vector3(3.5f,10-(lines*0.66f)-buttons,0), transform.rotation) as GameObject;
			newButton.GetComponent<button>().instruction = "Visit Hospital [+100hp, -60R]";
			newButton.GetComponent<button>().message = "goHos";
			newButton.GetComponent<button>().home = this.gameObject;
			buttons+=1;
			yield return new WaitForSeconds(0.3f);
		}
		if(!added){
			GameObject newButton;
			newButton = Instantiate(button, new Vector3(3.5f,10-(lines*0.66f)-buttons,0), transform.rotation) as GameObject;
			newButton.GetComponent<button>().instruction = "Visit Shop";
			newButton.GetComponent<button>().message = "goShop";
			newButton.GetComponent<button>().home = this.gameObject;
			buttons+=1;
			yield return new WaitForSeconds(0.3f);
		}
		if(!added){
			GameObject newButton;
			newButton = Instantiate(button, new Vector3(3.5f,10-(lines*0.66f)-buttons,0), transform.rotation) as GameObject;
			newButton.GetComponent<button>().instruction = "Go Home";
			newButton.GetComponent<button>().message = "goHome";
			newButton.GetComponent<button>().home = this.gameObject;
			buttons+=1;
			yield return new WaitForSeconds(0.3f);
		}
		
	}

	void goHos(){
		global control = GameObject.Find("Global").GetComponent<global>();
		control.money-=60;
		control.health=100;
		CameraFade.StartAlphaFade( Color.white, false, control.cameraFadeDuration, control.cameraFadeDelay, () => { Application.LoadLevel("meal"); } );
	}

	void goHome(){
		CameraFade.StartAlphaFade( Color.white, false, control.cameraFadeDuration, control.cameraFadeDelay, () => { Application.LoadLevel("meal"); } );
	}
	void goShop(){
		CameraFade.StartAlphaFade( Color.white, false, control.cameraFadeDuration, control.cameraFadeDelay, () => { Application.LoadLevel("shopScreen2"); } );
	}

}
