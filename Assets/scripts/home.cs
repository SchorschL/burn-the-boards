﻿using UnityEngine;
using System.Collections;

public class home : MonoBehaviour {

	public bool visit = false;
	public TextMesh saying;
	public GameObject button;
	private bool done = false;

	private global control;

	void Start(){
		control = GameObject.Find("Global").GetComponent<global>();
		GameObject.Find("food").GetComponent<TextMesh>().text = control.food + " food";
		string message = "Is there anything that you want me to do tomorrow?";
		//make visiting events start on this screen
		int random = Random.Range(0,6);

		if(random==4 && control.events[random]==0){
			visit=true;
			control.wifeTask=11;
			message = "My brother wants to visit tomorrow, is that alright?";
			control.events[random]=1;
		}
		if(random==5 && control.events[random]==0){
			visit=true;
			control.wifeTask=12;
			message = "I have to visit my aunt in hospital tomorrow.";
			control.events[random]=1;
		}

		StartCoroutine(saying.GetComponent<wrapText>().loadLine(message));


	}

	void Update(){
		if(saying.GetComponent<wrapText>().done && !done){
			
			StartCoroutine(addButtons());
			done = true;
		}
	}

	IEnumerator addButtons(){
		
		bool added = false;
		int lines = saying.GetComponent<wrapText>().lines;
		int buttons = 0;
		
		//brother visit
		if(visit && control.wifeTask==11 && control.food>=2){
			GameObject newButton;
			newButton = Instantiate(button, new Vector3(3.5f,10-(lines*0.66f)-buttons,0), transform.rotation) as GameObject;
			newButton.GetComponent<button>().instruction = "Of course. [-2 food]";
			newButton.GetComponent<button>().message = "brothervisit";
			newButton.GetComponent<button>().home = this.gameObject;
			buttons+=1;
			yield return new WaitForSeconds(0.3f);
		}
		if(visit && control.wifeTask==11){
			GameObject newButton;
			newButton = Instantiate(button, new Vector3(3.5f,10-(lines*0.66f)-buttons,0), transform.rotation) as GameObject;
			newButton.GetComponent<button>().instruction = "I'm sorry, he can't visit";
			newButton.GetComponent<button>().message = "noBro";
			newButton.GetComponent<button>().home = this.gameObject;
			buttons+=1;
			yield return new WaitForSeconds(0.3f);
		}
		//aunt visit
		if(visit && control.wifeTask==12){
			GameObject newButton;
			newButton = Instantiate(button, new Vector3(3.5f,10-(lines*0.66f)-buttons,0), transform.rotation) as GameObject;
			newButton.GetComponent<button>().instruction = "Okay, I hope that she is alright.";
			newButton.GetComponent<button>().message = "hope";
			newButton.GetComponent<button>().home = this.gameObject;
			buttons+=1;
			yield return new WaitForSeconds(0.3f);
		}
		//normal
		if(!visit){
			GameObject newButton;
			newButton = Instantiate(button, new Vector3(3.5f,10-(lines*0.66f)-buttons,0), transform.rotation) as GameObject;
			newButton.GetComponent<button>().instruction = "Please rest [+20 wife hp]";
			newButton.GetComponent<button>().message = "relax";
			newButton.GetComponent<button>().home = this.gameObject;
			buttons+=1;
			yield return new WaitForSeconds(0.3f);
		}
		if(!visit && control.money>=30){
			GameObject newButton;
			newButton = Instantiate(button, new Vector3(3.5f,10-(lines*0.66f)-buttons,0), transform.rotation) as GameObject;
			newButton.GetComponent<button>().instruction = "Please buy 2 food [30R]";
			newButton.GetComponent<button>().message = "Food2";
			newButton.GetComponent<button>().home = this.gameObject;
			buttons+=1;
			yield return new WaitForSeconds(0.3f);
		}
		if(!visit && control.money>=60){
			GameObject newButton;
			newButton = Instantiate(button, new Vector3(3.5f,10-(lines*0.66f)-buttons,0), transform.rotation) as GameObject;
			newButton.GetComponent<button>().instruction = "Please buy 10 food [60R]";
			newButton.GetComponent<button>().message = "Food4";
			newButton.GetComponent<button>().home = this.gameObject;
			buttons+=1;
			yield return new WaitForSeconds(0.3f);
		}
		if(!visit && control.money>=20){
			GameObject newButton;
			newButton = Instantiate(button, new Vector3(3.5f,10-(lines*0.66f)-buttons,0), transform.rotation) as GameObject;
			newButton.GetComponent<button>().instruction = "Minor home repairs [20R + 10S]";
			newButton.GetComponent<button>().message = "minorHome";
			newButton.GetComponent<button>().home = this.gameObject;
			buttons+=1;
			yield return new WaitForSeconds(0.3f);
		}
		if(!visit && control.money>=50){
			GameObject newButton;
			newButton = Instantiate(button, new Vector3(3.5f,10-(lines*0.66f)-buttons,0), transform.rotation) as GameObject;
			newButton.GetComponent<button>().instruction = "Please call a builder [50R + 40S]";
			newButton.GetComponent<button>().message = "builder";
			newButton.GetComponent<button>().home = this.gameObject;
			buttons+=1;
			yield return new WaitForSeconds(0.3f);
		}
	}


	void brothervisit(){
		control.food-=2;
		CameraFade.StartAlphaFade( Color.white, false, control.cameraFadeDuration, control.cameraFadeDelay, () => { Application.LoadLevel("morning"); } );
	}
	void noBro(){
		control.wifeTask=0;
		CameraFade.StartAlphaFade( Color.white, false, control.cameraFadeDuration, control.cameraFadeDelay, () => { Application.LoadLevel("morning"); } );
	}
	void hope(){
		CameraFade.StartAlphaFade( Color.white, false, control.cameraFadeDuration, control.cameraFadeDelay, () => { Application.LoadLevel("morning"); } );
	}
	void relax(){
		control.wifeHealth+=20;
		CameraFade.StartAlphaFade( Color.white, false, control.cameraFadeDuration, control.cameraFadeDelay, () => { Application.LoadLevel("morning"); } );
	}
	void Food2(){
		control.money-=10;
		control.food+=2;
		control.wifeTask=1;
		CameraFade.StartAlphaFade( Color.white, false, control.cameraFadeDuration, control.cameraFadeDelay, () => { Application.LoadLevel("morning"); } );
	}
	void Food4(){
		control.money-=40;
		control.food+=10;
		control.wifeTask=2;
		CameraFade.StartAlphaFade( Color.white, false, control.cameraFadeDuration, control.cameraFadeDelay, () => { Application.LoadLevel("morning"); } );
	}
	void minorHome(){
		control.money-=20;
		control.shelter+=10;
		control.wifeTask=3;
		CameraFade.StartAlphaFade( Color.white, false, control.cameraFadeDuration, control.cameraFadeDelay, () => { Application.LoadLevel("morning"); } );
	}
	void builder(){
		control.money-=50;
		control.shelter+=40;
		control.wifeTask=4;
		CameraFade.StartAlphaFade( Color.white, false, control.cameraFadeDuration, control.cameraFadeDelay, () => { Application.LoadLevel("morning"); } );
	}
}
