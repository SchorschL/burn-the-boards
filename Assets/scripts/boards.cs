﻿using UnityEngine;
using System.Collections;

public class boards : MonoBehaviour {

	public Sprite[] circuits;

	// Use this for initialization
	void Start () {
		//set a random boards
		GetComponent<SpriteRenderer>().sprite = circuits[Random.Range(0,circuits.Length)];
	}
}
