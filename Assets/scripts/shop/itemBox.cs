﻿using UnityEngine;
using System.Collections;

public class itemBox : MonoBehaviour {

	public GameObject item;
	public int value;
	public string desc = "";
	public string namer = "";

	public TextMesh price;
	public TextMesh descText;
	public TextMesh namerTest;
	public int itemID = 0;

	public GameObject itemUse;

	// Update is called once per frame
	void Update () {
	
		//show the info
		price.GetComponent<TextMesh>().text = value.ToString();
		descText.GetComponent<TextMesh>().text = desc;
		namerTest.GetComponent<TextMesh>().text = namer;

		global control = GameObject.Find("Global").GetComponent<global>();

		bool canBuy = false;

		if(itemID==0){
			GameObject.Find("buyText").GetComponent<TextMesh>().text = "Purchase";
		}

		//only allow purchase if the item hasnt already been bought
		if(itemID>0 && control.itemStore[itemID]==0){
			canBuy = true;
			GameObject.Find("buyText").GetComponent<TextMesh>().text = "Purchase";
		}

		//bought text
		if(itemID>0 && control.itemStore[itemID]!=0){
			GameObject.Find("buyText").GetComponent<TextMesh>().text = "Bought";
		}
	}

	 void buy(){

		global control = GameObject.Find("Global").GetComponent<global>();

		bool canBuy = false;

		//only allow purchase if the item hasnt already been bought
		if(itemID>0 && control.itemStore[itemID]==0){
			canBuy = true;
		}
		//unlimited purchase items
		if(itemID==0){
			canBuy = true;
		}

		if(control.money>=value && canBuy){
			control.money-=value;

			//move the menu back
			Vector3 storePos = GameObject.Find("itemBox").transform.position;
			GameObject.Find("itemBox").transform.position = new Vector3(-10,storePos.y,storePos.z);

			//if needed, mark it as bought
			if(itemID>0){
				control.itemStore[itemID]=1;
			}

			//doing some item effects (when they are singles)
			if(itemID==2){
				print ("buy candle");
				//candle effect here
			}
			if(itemID==3){
				print ("buy chisel");
				control.maxSwipe+=1;
			}
			if(itemID==4){
				print ("buy hammer");
				control.maxSwipe+=1;
			}
			if(itemID==5){
				print ("buy lightbulb");
				control.healPerDay+=5;
			}
			if(itemID==6){
				print ("buy mask");
				control.defense+=2;
			}
			if(itemID==7){
				print ("buy sink");
				control.healPerDay+=1;
			}

			//premium items
			if(itemID==8){
				print ("buy gold lightbulb");
				control.healPerDay+=10;
			}
			if(itemID==9){
				print ("buy gold mask");
				control.defense+=5;
			}

			//make a sound and a simple use item deelybob
			if(itemUse!=null){

				GameObject newItem;
				newItem = Instantiate(itemUse, transform.position, transform.rotation) as GameObject;

				//show a thanks message from mr chopra to confirm the purchase
			}

		}
	}
}