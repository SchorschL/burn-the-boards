﻿using UnityEngine;
using System.Collections;

public class itemReelArrow : MonoBehaviour {

	public itemReel reel;
	public bool left = false;

	// Use this for initialization
	void OnMouseDown(){

		if(left){
			reel.MoveLeft();
		}
		else{
			reel.MoveRight();
		}

	}
}
