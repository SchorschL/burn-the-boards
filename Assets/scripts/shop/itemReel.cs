﻿using UnityEngine;
using System.Collections;

public class itemReel : MonoBehaviour {

	public GameObject[] reels;
	public GameObject[] items;

	public GameObject holder;
	public int starter = 0;

	// Use this for initialization
	void Start () {
	
		AddItems();
	}

	void AddItems(){

		if(holder!=null){
			Destroy(holder);
		}
		
		holder = new GameObject();
		int item = 0;

		item = starter;
		//add the items to the corrent reel
		for (int i = 0; i < reels.Length; i++) {

			item += 1;

			if(item>=items.Length){
				item = 0;
			}

			//Stuff
			GameObject clone;
			clone = Instantiate(items[item],reels[i].transform.position,reels[i].transform.rotation) as GameObject;
			clone.transform.parent = holder.gameObject.transform;

			clone.name = "item - " + (item);
		}
	}
	
	public void MoveLeft(){
		if(starter>=0){
			starter-=1;
		}
		//knock the starter to the end of the list
		if(starter<0){
			starter = items.Length-1;
		}

		AddItems();
	}
	public void MoveRight(){
		starter+=1;
		if(starter>=items.Length){
			starter = 0;
		}
		AddItems();
	}
}