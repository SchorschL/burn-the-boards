﻿using UnityEngine;
using System.Collections;

public class shopItem : MonoBehaviour {

	//show the item when clicked on
	public GameObject thisItem;
	public int price = 100;
	public string desc = "";
	public string name = "";
	public int itemID = 0;

	
	void OnMouseDown(){
		Vector3 storePos = GameObject.Find("itemBox").transform.position;
		GameObject.Find("itemBox").transform.position = new Vector3(3.5f,storePos.y,storePos.z);
		GameObject.Find("itemBox").GetComponent<itemBox>().item = thisItem;
		GameObject.Find("itemBox").GetComponent<itemBox>().value = price;
		GameObject.Find("itemBox").GetComponent<itemBox>().desc = desc;
		GameObject.Find("itemBox").GetComponent<itemBox>().namer = name;
		GameObject.Find("itemBox").GetComponent<itemBox>().itemID = itemID;
		GameObject.Find("itemBox").GetComponent<itemBox>().itemUse = thisItem;
	}
}
