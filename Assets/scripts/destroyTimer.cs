﻿using UnityEngine;
using System.Collections;

public class destroyTimer : MonoBehaviour {

	public bool scale = true;

	// Use this for initialization
	void Start () {

		if(scale){
			float value = 0.5f+(Random.value*0.5f);
			transform.localScale = new Vector3(value,value,value);
		}

		StartCoroutine (timing());
	}
	
	IEnumerator timing(){
		yield return new WaitForSeconds(0.1f);
		Destroy(gameObject);
	}
}
